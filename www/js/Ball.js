class Ball extends GameObject
{
    constructor(ballImage, positionX, positionY, ballWidth, ballHeight)
    {
        super(10);

        this.startX = positionX;
        this.startY = positionY;
        this.x = this.startX;
        this.y = this.startY;

        this.ballImage = ballImage;
        this.width = ballWidth;
        this.height = ballHeight;
        this.setDirection(STOPPED);
    }

    updateState()
    {
        if (this.direction === UP)
        {
            this.y--;
        }
        else if (this.direction === LEFT)
        {
            this.x--;
        }
        else if (this.direction === DOWN)
        {
            this.y++;
        }
        else if (this.direction === RIGHT)
        {
            this.x++;
        }
        else if (this.direction === UPLEFT)
        {
            this.y--;
            this.x--;
        }
        else if (this.direction === UPRIGHT)
        {
            this.y--;
            this.x++;
        }
        else if (this.direction === DOWNLEFT)
        {
            this.y++;
            this.x--;
        }
        else if (this.direction === DOWNRIGHT)
        {
            this.y++;
            this.x++;
        }
    }

    render()
    {
        ctx.drawImage(this.ballImage, this.x, this.y, this.width, this.height);
    }

    setDirection(newDirection)
    {
        if (this.direction !== START)
        {
            this.direction = newDirection;
        }
        else //Spacebar = reset to start
        {
            this.x = this.startX;
            this.y = this.startY;
            this.direction = STOPPED;
        }
    }

    getDirection()
    {
        return(this.direction);
    }

    getX()
    {
        return this.x;
    }

    getY()
    {
        return this.y;
    }

    setX(newX)
    {
        this.x = newX;
    }

    setY(newY)
    {
        this.y = newY;
    }

    getWidth()
    {
        return this.width;
    }

    getHeight()
    {
        return this.height;
    }
}
