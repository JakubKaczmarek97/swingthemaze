class MazeCanvasGame extends CanvasGame {
    constructor(mazeGridImage) {
        super();

        /* this.mazeCtx will be used for collision detection */
        let mazeOffscreenCanvas = document.createElement('canvas');
        this.mazeCtx = mazeOffscreenCanvas.getContext('2d');
        mazeOffscreenCanvas.width = canvas.width;
        mazeOffscreenCanvas.height = canvas.height;
        this.mazeCtx.drawImage(mazeGridImage, 0, 0, canvas.width, canvas.height);
    }

    collisionDetection() {
        if (!this.mazeCtx) {
            return;
        }

        if (gameObjects[BALL].getY() > canvas.height) { //Finish level and update high scores
            clearInterval(INTERVAL);
            if (CURRENT_LEVEL === 1) {
                if(SECONDS < LEVEL_1_SCORE) {
                    LEVEL_1_SCORE = SECONDS;
                }
            }
            else if (CURRENT_LEVEL === 2) {
                if(SECONDS < LEVEL_2_SCORE) {
                    LEVEL_2_SCORE = SECONDS;
                }
            }
            else if (CURRENT_LEVEL === 3) {
                if(SECONDS < LEVEL_3_SCORE) {
                    LEVEL_3_SCORE = SECONDS;
                }
            }
            db.collection('highscores').doc('score').update({
                level1: LEVEL_1_SCORE,
                level2: LEVEL_2_SCORE,
                level3: LEVEL_3_SCORE
            })
            SECONDS = 0;
            CURRENT_LEVEL = 0;
            playGame();
        }

        let imageData = this.mazeCtx.getImageData(gameObjects[BALL].getX(), gameObjects[BALL].getY(), 1, 1);
        let dataTop = imageData.data;
        imageData = this.mazeCtx.getImageData(gameObjects[BALL].getX() + gameObjects[BALL].getWidth() * 0.9, gameObjects[BALL].getY(), 1, 1);
        let dataRight = imageData.data;
        imageData = this.mazeCtx.getImageData(gameObjects[BALL].getX(), gameObjects[BALL].getY() + gameObjects[BALL].getHeight() * 0.9, 1, 1);
        let dataBottom = imageData.data;
        imageData = this.mazeCtx.getImageData(gameObjects[BALL].getX() + gameObjects[BALL].getWidth() * 0.7, gameObjects[BALL].getY() + gameObjects[BALL].getHeight() * 0.7, 1, 1);
        let dataLeft = imageData.data;

        if ((dataTop[3] !== 0) || (dataRight[3] !== 0) || (dataBottom[3] !== 0) || (dataLeft[3] !== 0)) {
            if (gameObjects[BALL].getDirection() === UP) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setY(gameObjects[BALL].getY() + 3);
            } else if (gameObjects[BALL].getDirection() === DOWN) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setY(gameObjects[BALL].getY() - 3);
            } else if (gameObjects[BALL].getDirection() === LEFT) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setX(gameObjects[BALL].getX() + 3);
            } else if (gameObjects[BALL].getDirection() === RIGHT) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setX(gameObjects[BALL].getX() - 3);
            } else if (gameObjects[BALL].getDirection() === UPLEFT) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setY(gameObjects[BALL].getY() + 3);
                gameObjects[BALL].setX(gameObjects[BALL].getX() + 3);
            } else if (gameObjects[BALL].getDirection() === UPRIGHT) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setY(gameObjects[BALL].getY() + 3);
                gameObjects[BALL].setX(gameObjects[BALL].getX() - 3);
            } else if (gameObjects[BALL].getDirection() === DOWNLEFT) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setY(gameObjects[BALL].getY() - 3);
                gameObjects[BALL].setX(gameObjects[BALL].getX() + 3);
            } else if (gameObjects[BALL].getDirection() === DOWNRIGHT) {
                gameObjects[BALL].setDirection(STOPPED);
                gameObjects[BALL].setY(gameObjects[BALL].getY() - 3);
                gameObjects[BALL].setX(gameObjects[BALL].getX() - 3);
            }
        }
    }
}
