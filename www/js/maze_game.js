/******************** Declare game specific global data and functions *****************/
/* images must be declared as global, so that they will load before the game starts  */

let soccerBallImage = new Image();
soccerBallImage.src = "images/soccerBall.png";

let tennisBallImage = new Image();
tennisBallImage.src = "images/tennisBall.png";

let basketballBallImage = new Image();
basketballBallImage.src = "images/basketballBall.png";

let stadiumBackground = new Image();
stadiumBackground.src = "images/stadiumBackground.png";

let tennisCourtBackground = new Image();
tennisCourtBackground.src = "images/tennisCourtBackground.jpg";

let basketballBackground = new Image();
basketballBackground.src = "images/basketballBackground.jpg";

let menu_background = new Image();
menu_background.src = "images/menu_background.jpg";

let mazeGridFrame = new Image();
mazeGridFrame.src = "images/maze_grid_frame.png";

let mazeGridLevel1 = new Image();
mazeGridLevel1.src = "images/maze_grid_level_1.png";

let mazeGridLevel2 = new Image();
mazeGridLevel2.src = "images/maze_grid_level_2.png";

let mazeGridLevel3 = new Image();
mazeGridLevel3.src = "images/maze_grid_level_3.png";

/* BALL DIRECTIONS */
const UP = 0;
const LEFT = 1;
const DOWN = 2;
const RIGHT = 3;
const UPLEFT = 4;
const UPRIGHT = 5;
const DOWNLEFT = 6;
const DOWNRIGHT = 7;
const STOPPED = 8;
const START = 9;

/* Variables for timer and store current level time */
let SECONDS = 0; //Time of current level in seconds
let INTERVAL = 0; //Variable for setInterval function
let isMainMenu = true;
let LEVEL_1_SCORE = 0;
let LEVEL_2_SCORE = 0;
let LEVEL_3_SCORE = 0;
let CURRENT_LEVEL = 0;

/* The various gameObjects */
/* These are the positions that each gameObject is held in the gameObjects[] array */
const BACKGROUND = 0;
const MAZE = 1;
const BALL = 2;
const LEVEL_1_BUTTON = 3;
const LEVEL_2_BUTTON = 4;
const LEVEL_3_BUTTON = 5;
const HIGH_SCORES_BUTTON = 6;
const EXIT_BUTTON = 7;
const TEXT_LEVEL_1 = 8;
const TEXT_LEVEL_2 = 9;
const TEXT_LEVEL_3 = 10;
/******************* END OF Declare game specific data and functions *****************/

function playGame() {
    /* Get highscores from firestore database */

    let docRef = db.collection("highscores").doc("score");

    docRef.get().then(function(doc) {
        if (doc.exists) {
            LEVEL_1_SCORE = doc.data().level1;
            LEVEL_2_SCORE = doc.data().level2;
            LEVEL_3_SCORE = doc.data().level3;
        } else {
            console.log("No such document!");
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });

    gameObjects[LEVEL_1_BUTTON] = new Button(BUTTON_CENTRE, 50, TEXT_WIDTH, TEXT_HEIGHT, "Level 1");
    gameObjects[LEVEL_2_BUTTON] = new Button(BUTTON_CENTRE, 150, TEXT_WIDTH, TEXT_HEIGHT, "Level 2");
    gameObjects[LEVEL_3_BUTTON] = new Button(BUTTON_CENTRE, 250, TEXT_WIDTH, TEXT_HEIGHT, "Level 3");
    gameObjects[EXIT_BUTTON] = new Button();
    gameObjects[HIGH_SCORES_BUTTON] = new Button(BUTTON_CENTRE, 350, TEXT_WIDTH, TEXT_HEIGHT, "High Scores");
    gameObjects[BACKGROUND] = new StaticImage(menu_background, 0, 0, canvas.width, canvas.height);
    gameObjects[TEXT_LEVEL_1] = new StaticText("", 0,0, "Times Roman", 0, "black");
    gameObjects[TEXT_LEVEL_2] = new StaticText("", 0,0, "Times Roman", 0, "black");
    gameObjects[TEXT_LEVEL_3] = new StaticText("", 0,0, "Times Roman", 0, "black");
    gameObjects[MAZE] = new StaticImage(mazeGridFrame, 0, 0, 0, 0);
    gameObjects[BALL] = new Ball(soccerBallImage, 0, 0, 0, 0);

    let game = new CanvasGame();
    game.start();

    /* MOUSEDOWN LISTENER */
    document.getElementById("gameCanvas").addEventListener("mousedown", function (e)
    {
        if (e.which === 1)  // left mouse button
        {
            let canvasBoundingRectangle = document.getElementById("gameCanvas").getBoundingClientRect();
            let mouseX = e.clientX - canvasBoundingRectangle.left;
            let mouseY = e.clientY - canvasBoundingRectangle.top;

            if (gameObjects[LEVEL_1_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY) ||
                gameObjects[LEVEL_2_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY) ||
                gameObjects[LEVEL_3_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY) ||
                gameObjects[HIGH_SCORES_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY) ||
                gameObjects[EXIT_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY))
            {
                if (gameObjects[LEVEL_1_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY))
                {
                    CURRENT_LEVEL = 1;
                    gameObjects[BACKGROUND] = new StaticImage(stadiumBackground, 0, 0, canvas.width, canvas.height);
                    gameObjects[MAZE] = new StaticImage(mazeGridLevel1, 0, 0, canvas.width, canvas.height);
                    gameObjects[BALL] = new Ball(soccerBallImage, 20, 20, 30, 30);
                    gameObjects[EXIT_BUTTON] = new Button(250, 1, 50, 25, "Exit", true, null, 20, "Times Roman", "black");
                    isMainMenu = false;
                    game = new MazeCanvasGame(mazeGridLevel1);
                }
                else if (gameObjects[LEVEL_2_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY))
                {
                    CURRENT_LEVEL = 2;
                    gameObjects[BACKGROUND] = new StaticImage(tennisCourtBackground, 0, 0, canvas.width, canvas.height);
                    gameObjects[MAZE] = new StaticImage(mazeGridLevel2, 0, 0, canvas.width, canvas.height);
                    gameObjects[BALL] = new Ball(tennisBallImage, 20, 20, 30, 30);
                    gameObjects[EXIT_BUTTON] = new Button(250, 1, 50, 25, "Exit", true, null, 20, "Times Roman", "black");
                    isMainMenu = false;
                    game = new MazeCanvasGame(mazeGridLevel2);
                }
                else if (gameObjects[LEVEL_3_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY))
                {
                    CURRENT_LEVEL = 3;
                    gameObjects[BACKGROUND] = new StaticImage(basketballBackground, 0, 0, canvas.width, canvas.height);
                    gameObjects[MAZE] = new StaticImage(mazeGridLevel3, 0, 0, canvas.width, canvas.height);
                    gameObjects[BALL] = new Ball(basketballBallImage, 20, 20, 30, 30);
                    gameObjects[EXIT_BUTTON] = new Button(250, 1, 50, 25, "Exit", true, null, 20, "Times Roman", "black");
                    isMainMenu = false;
                    game = new MazeCanvasGame(mazeGridLevel3);
                }
                else if (gameObjects[HIGH_SCORES_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY)) {
                    gameObjects[BALL] = new Ball(basketballBallImage, 0,0,0,0);
                    gameObjects[EXIT_BUTTON] = new Button(250, 1, 50, 25, "Exit", true, null, 20, "Times Roman", "black");
                    isMainMenu = false;
                    gameObjects[TEXT_LEVEL_1] = new StaticText("Level1: " + LEVEL_1_SCORE + "s", 20, 100, "Times Roman", 50, "red");
                    gameObjects[TEXT_LEVEL_1].start();
                    gameObjects[TEXT_LEVEL_2] = new StaticText("Level2: " + LEVEL_2_SCORE + "s", 20, 200, "Times Roman", 50, "red");
                    gameObjects[TEXT_LEVEL_2].start();
                    gameObjects[TEXT_LEVEL_3] = new StaticText("Level3: " + LEVEL_3_SCORE + "s", 20, 300, "Times Roman", 50, "red");
                    gameObjects[TEXT_LEVEL_3].start();
                    game = new CanvasGame();
                }
                else if (gameObjects[EXIT_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY)) {
                    gameObjects[BALL] = new StaticImage(mazeGridFrame, 0,0,0,0);
                    playGame();
                    isMainMenu = true;
                }

                if(!isMainMenu) {
                    gameObjects[LEVEL_1_BUTTON] = new Button();
                    gameObjects[LEVEL_2_BUTTON] = new Button();
                    gameObjects[LEVEL_3_BUTTON] = new Button();
                    gameObjects[HIGH_SCORES_BUTTON] = new Button();
                }

                game.start();

                INTERVAL = setInterval(function () {
                    SECONDS++;
                }, 1000);
            }
        }
    });

    document.getElementById("gameCanvas").addEventListener("mousemove", function (e)
    {
        if (e.which === 0) // no button selected
        {
            let canvasBoundingRectangle = document.getElementById("gameCanvas").getBoundingClientRect();
            let mouseX = e.clientX - canvasBoundingRectangle.left;
            let mouseY = e.clientY - canvasBoundingRectangle.top;

            gameObjects[LEVEL_1_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY);
            gameObjects[LEVEL_2_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY);
            gameObjects[LEVEL_3_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY);
            gameObjects[EXIT_BUTTON].pointIsInsideBoundingRectangle(mouseX, mouseY);
        }
    });


    window.addEventListener("deviceorientation", function (event) {
        let x = event.beta;  // In degree in the range [-180,180)
        let y = event.gamma; // In degree in the range [-90,90)

        // Because we don't want to have the device upside down
        // We constrain the x value to the range [-90,90]
        if (x > 90) {
            x = 90;
        }
        if (x < -90) {
            x = -90;
        }

        // To make computation easier we shift the range of
        // x and y to [0,180]
        x += 90;
        y += 90;

		if(x>100) {
		    if (!(y<90 || y>100)) {
                gameObjects[BALL].setDirection(DOWN);
            }
			else if (y<90) {
                gameObjects[BALL].setDirection(DOWNLEFT);
            }
			else {
                gameObjects[BALL].setDirection(DOWNRIGHT);
            }
		}
		else if (x<90) {
            if (!(y<90 || y>100)) {
                gameObjects[BALL].setDirection(UP);
            }
            else if (y<90) {
                gameObjects[BALL].setDirection(UPLEFT);
            }
            else {
                gameObjects[BALL].setDirection(UPRIGHT);
            }
		}
		else if (y>100) {
            if (!(x<90 || x>100)) {
                gameObjects[BALL].setDirection(RIGHT);
            }
            else if (x<90) {
                gameObjects[BALL].setDirection(UPRIGHT);
            }
            else {
                gameObjects[BALL].setDirection(DOWNRIGHT);
            }
		}
		else if (y<90) {
		    if (!(x<90 || x>100)) {
                gameObjects[BALL].setDirection(LEFT);
            }
		    else if (x<90) {
                gameObjects[BALL].setDirection(UPLEFT);
            }
		    else {
                gameObjects[BALL].setDirection(DOWNLEFT);
            }
		}
		else {
			gameObjects[BALL].setDirection(STOPPED);
		}
    });

    let keys;

    document.addEventListener("keydown", function (e) {
        keys = (keys || []);
        keys[e.keyCode] = true;

        whichButtonsPressed();

    }, false);

    document.addEventListener("keyup", function (e) {
        keys[e.keyCode] = false;
        stop();

        function isFalse(index) {
            return index === false;
        }

        if(keys.every(isFalse)) {
            gameObjects[BALL].setDirection(STOPPED);
        }
        else {
            whichButtonsPressed();
        }
    }, false);

    function whichButtonsPressed() {
        //37: left, 38: up, 39: right, 40: down

        if (keys[37]) {
            if (!(keys[38] || keys[39] || keys[40])) {
                gameObjects[BALL].setDirection(LEFT);
            }
            else if(keys[38] && !keys[40]) {
                gameObjects[BALL].setDirection(UPLEFT);
            }
            else {
                gameObjects[BALL].setDirection(DOWNLEFT);
            }
        }
        else if (keys[38]) {
            if (!(keys[37] || keys[39] || keys[40])) {
                gameObjects[BALL].setDirection(UP);
            }
            else if (keys[37] && !keys[39]) {
                gameObjects[BALL].setDirection(UPLEFT);
            }
            else {
                gameObjects[BALL].setDirection(UPRIGHT);
            }
        }
        else if (keys[39]) {
            if (!(keys[37] || keys[38] || keys[40])) {
                gameObjects[BALL].setDirection(RIGHT);
            }
            else if(keys[38] && !keys[40]) {
                gameObjects[BALL].setDirection(UPRIGHT);
            }
            else {
                gameObjects[BALL].setDirection(DOWNRIGHT);
            }
        }
        else if (keys[40]) {
            if (!(keys[37] || keys[38] || keys[39])) {
                gameObjects[BALL].setDirection(DOWN);
            }
            else if (keys[37] && !keys[39]) {
                gameObjects[BALL].setDirection(DOWNLEFT);
            }
            else {
                gameObjects[BALL].setDirection(DOWNRIGHT);
            }
        }
        else if (keys[32]) {
            gameObjects[BALL].setDirection(START);
        }
    }
}
